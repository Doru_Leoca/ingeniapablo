#pragma once

#include <string>
#include "SDL.h"
#include "SDL_mixer.h"
#include "MACRO_MOSTRAR.h"
//#define MOSTRAR_SONIDO 1
#include <stdio.h>

class AudioItemChunk
{
private:
	unsigned int error;
	std::string path;
	Mix_Chunk *wave = NULL;
	int channel;
public:
	AudioItemChunk();
	AudioItemChunk(std::string p);
	~AudioItemChunk();
	unsigned int getError() { return error; }
	void setPath(std::string str);
	std::string getPath() { return path; }
	void play(int veces);
	void pause();
	void resume();
	bool isPlaying();
	bool isPaused();
	int setVolume(int volume);
};

