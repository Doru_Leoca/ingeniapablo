#pragma once

#include "FuncionesAyuda.h"	
//#include "constantes.h"
#include "punto.h"
#include "Inventario.h"
#include <math.h>
#include "MACRO_MOSTRAR.h"
//#define MOSTRAR_MOVIMIENTO 1

class Humano : public Punto
{
	private:
		double direccion; //�ngulo en radianes
		bool avance;
		bool correr;
		bool retroceso;
		bool giro_izq;
		bool giro_der;
		bool salto;
		int estado_salto; //estado cuando empieza a saltar (caminando, corriendo, retrocediendo o parado)
		bool volando;
		int estado_volando;  //estado cuando empieza a saltar (caminando, corriendo, retrocediendo o parado)
		double velx;
		double vely;
		double velz;
		Inventario mochila;
		bool inventarioCambiado;


	public:
		Humano(double a, double b, double c, double teta);
		//void girar(double teta);
		//void avanzar(double x);
		void setAvance(bool a) { avance = a; };
		void setCorrer(bool a) { correr = a; };
		void setRetroceso(bool a) {retroceso = a; };
		void setGiroIzq(bool a) { giro_izq = a; };
		void setGiroDer(bool a) { giro_der = a; };
		void setSalto(bool a);
		void setEstadoSalto(int e) { estado_salto = e; }
		void setVolando(bool a);
		void setEstadoVolando(int e) { estado_volando = e; }
		void startSalto(bool a);
		bool getAvance() { return avance; };
		bool getCorrer() { return correr; };
		bool getRetroceso() { return retroceso; };
		bool getSalto() { return salto; };
		int getEstadoSalto() { return estado_salto; }
		bool getVolando() { return salto; };
		int getEstadoVolando() { return estado_salto; }
		void actua(double t, osg::Image* image, unsigned int colision[8]);
		void setDireccion(double teta);
		double getDireccion();
		void setVelZ(double vz) { velz = vz; };
		double getVelZ() { return velz; };
		bool enMovimiento();
		double distanciaA(Punto p);
		bool estaCercaDe(Punto p, double Dmax);
		void setPosition(double a, double b, double c, double d);

		void a�adirObjeto(Objeto obj);
		int quitarObjeto(Objeto obj);
		int eliminarObjeto(Objeto obj);
		int poseeObjeto(Objeto obj);
		Objeto* encuentraObjeto(Objeto obj);
		void imprimirInventario();
		Inventario* getInventario();
		void setInventario(Inventario* inv);
		bool getInventarioCambiado() { return inventarioCambiado; };
		void setInventarioCambiado(bool b) { inventarioCambiado = b; };
		void vaciarInventario();
};