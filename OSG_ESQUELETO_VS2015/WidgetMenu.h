// -*-c++-*- osgWidget - Code by: Jeremy Moles (cubicool) 2007-2008
// $Id: osgwidgetmenu.cpp 66 2008-07-14 21:54:09Z cubicool $

#include <iostream>
#include <osgDB/ReadFile>
#include <osgWidget/Util>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Label>

// For now this is just an example, but osgWidget::Menu will later be it's own Window.
// I just wanted to get this out there so that people could see it was possible.

//const unsigned int MASK_2D = 0xF0000000;
//const unsigned int MASK_3D = 0x0F000000;

#define MENU_OPCION1 "EMPEZAR PARTIDA NUEVA"
#define MENU_OPCION2 "CONTINUAR PARTIDA GUARDADA"
#define MENU_OPCION3 "ESTADÍSTICAS"
#define MENU_OPCION4 "CRÉDITOS"
#define MENU_OPCION5 "SALIR"

#define OPCION0 ""
#define OPCION1 "Empezar de nuevo"
#define OPCION2 "Guardar"
#define OPCION3 "Volver sin guardar"
#define OPCION4 "Salir sin guardar"
#define OPCION5 "Ayuda"

#include "colores.h"

struct SonidoLabel : public osgWidget::Label {
	bool clicado = false;
	std::string name;
	bool encendido;
	SonidoLabel(const char* label);

	void setOn();
	void setOff();

	void reiniciarClick();

	bool mousePush(double, double, const osgWidget::WindowManager*);

	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	bool mouseLeave(double, double, const osgWidget::WindowManager*);
};

struct VolverLabel : public osgWidget::Label {
	bool clicado = false;
	VolverLabel(const char* label) :
		osgWidget::Label("", "") {
			setFont("./Otros/fonts/arial.ttf");
			setFontSize(14);
			setFontColor(COLOR_NEGRO);
			setColor(COLOR_GRIS_CLARITO);
			setCanFill(true);
			setLabel(label);
			setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_TOP);
			setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
	}

	void reiniciarClick() {
		clicado = false;
	}

	bool mousePush(double, double, const osgWidget::WindowManager*) {
		clicado = true;
		printf("clicado: volver\n");
		return true;
	}

	bool mouseEnter(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_BLANCO);
		return true;
	}

	bool mouseLeave(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_GRIS_CLARITO);
		return true;
	}
};

struct ColorLabel: public osgWidget::Label {
	bool overme = false;
	bool clicado = false;
	ColorLabel(const char* label);

	void reiniciarClick();

	bool mousePush(double, double, const osgWidget::WindowManager*);

	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	bool mouseLeave(double, double, const osgWidget::WindowManager*);
};

class ColorLabelMenu: public ColorLabel {
public:
	osg::ref_ptr<osgWidget::Window> _window;
	ColorLabel* c0;
	ColorLabel* c1;
	ColorLabel* c2;
	ColorLabel* c3;
	ColorLabel* c4;
	ColorLabel* c5;

public:
	ColorLabelMenu(const char* label);

	void reiniciarClick(int n = -1);

	void managed(osgWidget::WindowManager* wm);

	void positioned();

	bool mousePush(double, double, const osgWidget::WindowManager*);

	bool mouseLeave(double, double, const osgWidget::WindowManager*);
	bool mouseEnter(double, double, const osgWidget::WindowManager*);
};

struct MenuLabel : public osgWidget::Label {
	bool clicado = false;
	MenuLabel(const char* label);

	void reiniciarClick();

	bool mousePush(double, double, const osgWidget::WindowManager*);

	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	bool mouseLeave(double, double, const osgWidget::WindowManager*);
};

struct PreguntaLabel : public osgWidget::Label {
	PreguntaLabel(const char* pers, const char* frase, float length, float height);
};

struct RespuestaLabel : public osgWidget::Label {
	bool clicado = false;
	int respuesta = 0;
	RespuestaLabel(int r, const char* texto, float length, float height);

	void reiniciarClick();

	bool mousePush(double, double, const osgWidget::WindowManager*);

	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	bool mouseLeave(double, double, const osgWidget::WindowManager*);
};
std::string ajustarString(std::string s, int n);

struct SentenciaLabel : public osgWidget::Label {
	SentenciaLabel(const char* frase, float length, float height);
};

struct OpcionLabel : public osgWidget::Label {
	bool clicado = false;
	OpcionLabel(const char* texto, float length, float height);

	void reiniciarClick();

	bool mousePush(double, double, const osgWidget::WindowManager*);

	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	bool mouseLeave(double, double, const osgWidget::WindowManager*);
};

osgWidget::Label* createLabel(const std::string& text);

struct ObjetoWidget : public osgWidget::Widget {
	std::string nombre;
	ObjetoWidget(std::string name, float length, float height);

	std::string elegirImagenObjeto(std::string name);
};

struct NumeroWidget : public osgWidget::Widget {
	int numero;
	int numero_ant;
	double length;
	double height;
	NumeroWidget(int number, float l, float h);

	void setNumero(int n);

	std::string elegirImagenNumero(int n);
};
