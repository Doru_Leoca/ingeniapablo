#include "stdafx.h"
#include "Layout.h"
osg::MatrixTransform*escenario (std::string dir, float posx, float posy, float posz, float posfi, float esc)// Crea matriz 
{
	osg::Node* nodosuelo = osgDB::readNodeFile(dir);
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->addChild(nodosuelo);
	osg::Matrixd mtras,mrotz,mrotx,mroty,mesc,mtotal;
	osg::Vec3d pos(posx,posy,posz);
	mtras = mtras.translate(pos);
	mrotz = mrotz.rotate(posfi,osg::Vec3d(0,0,1));
		

	mesc = mesc.scale(esc,esc,esc);
		
		mtotal = mesc*mrotz*mtras;

		mt->setMatrix(mtotal);
		return mt;
}

  osg::MatrixTransform*escenario_duplicado (osg::Node* nodosuelo, float posx, float posy, float posz, float posfi, float esc)// Crea matriz 
{
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->addChild(nodosuelo);
	osg::Matrixd mtras,mrotz,mrotx,mroty,mesc,mtotal;
	osg::Vec3d pos(posx,posy,posz);
	mtras = mtras.translate(pos);
	mrotz = mrotz.rotate(posfi,osg::Vec3d(0,0,1));
	mesc = mesc.scale(esc,esc,esc);
	mtotal = mesc*mrotz*mtras;
	mt->setMatrix(mtotal);
		return mt;
}