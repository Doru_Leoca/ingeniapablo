#pragma once

#include <string>
#include <osg/Image>
#include <osg/Texture>
#include "colores.h"
//#include "constantes.h"

std::string porqueEstoyMuerto(std::string fase);

//estas funciones se han pasado a la clase Mapa
void youAreAt(double x, double y, osg::ref_ptr<osg::Image> imagen, const unsigned int puntos);
void youAreNotAt(float x, float y, osg::ref_ptr<osg::Image> imagen, osg::ref_ptr<osg::Image> imagen2, const unsigned int puntos);
void youAreAt2(double x, double y, double dir, osg::ref_ptr<osg::Image> imagen, osg::ref_ptr<osg::Image> imagen2);
void youAreNotAt2(float x, float y, osg::ref_ptr<osg::Image> imagen, osg::ref_ptr<osg::Image> imagen2);



std::vector<std::string> get_all_files_names_within_folder(std::string folder);
bool ordenarPorFase(std::pair<std::string,int> p1, std::pair<std::string,int> p2);
std::vector<std::pair<std::string,int>> obtenerEstadisticas();