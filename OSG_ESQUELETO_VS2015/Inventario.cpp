#include "Inventario.h"

Objeto::Objeto()
{
}

Objeto::Objeto(std::string n, Punto p=Punto(0,0,0), unsigned int c=0) : Punto(p)
{
	nombre = n;
	cantidad = c;
}

Objeto::Objeto(std::string n, double x, double y, double z, unsigned int c = 0)
{
	nombre = n;
	setX(x);
	setY(y);
	setZ(z);
	cantidad = c;
}

Objeto::Objeto(std::string n, unsigned int c)
{
	nombre = n;
	setX(0);
	setY(0);
	setZ(0);
	cantidad = c;
}

Objeto::~Objeto()
{
}

void Objeto::removeQuantity(unsigned int n) {
	if (cantidad>n)
		cantidad -= n;
	else
		cantidad = 0;
}

int Objeto::operator== (const Objeto& b)
{
	if (nombre == b.nombre)
		return 1;
	else
		return 0;
}



Inventario::Inventario()
{
}


Inventario::~Inventario()
{
}

void Inventario::a�adirObjeto(Objeto obj)
{
	Objeto* o = encuentraObjeto(obj);
	if (o != NULL)
		(*o).addQuantity(obj.getQuantity());
	else
		lista.push_back(obj);
}

int Inventario::quitarObjeto(Objeto obj)
{
	Objeto* o = encuentraObjeto(obj);
	if (o != NULL) { //si lo tiene
		(*o).removeQuantity(obj.getQuantity());
		if ((*o).getQuantity() == 0) {
			lista.remove((*o));
		}
		return 1;
	}
	else
		return 0;
}

int Inventario::eliminarObjeto(Objeto obj)
{
	Objeto* o = encuentraObjeto(obj);
	if (o != NULL) { //si lo tiene
		lista.remove((*o));
		return 1;
	}
	else
		return 0;
}

int Inventario::poseeObjeto(Objeto obj)
{
	return (std::find(lista.begin(), lista.end(), obj) != lista.end());
}

Objeto* Inventario::encuentraObjeto(Objeto obj)
{
	std::list<Objeto>::iterator it = std::find(lista.begin(), lista.end(), obj); //devuelve el objeto que coincide, o sino el �ltimo elemento de la lista
	if (it != lista.end()) //list::end no devuelve el �ltimo elemento, sino el elemento que estar�a despu�s del �ltimo elemento de la lista
		return &*it;
	else
		return NULL;
}

void Inventario::imprimirLista()
{
	std::list<Objeto>::iterator it = lista.begin();
	printf("\nlista:\n");
	for (it; it != lista.end(); it++) {
		printf("\t%s, (%lf,%lf,%lf) , %dud\n", it->getName().c_str(), it->getX(), it->getY(), it->getZ(), it->getQuantity());
	}
}

void imprimirLista(std::list<Objeto> lista)
{
	std::list<Objeto>::iterator it = lista.begin();
	printf("\nlista:\n");
	for (it; it != lista.end(); it++) {
		printf("\t%s, (%lf,%lf,%lf) , %dud\n", it->getName().c_str(), it->getX(), it->getY(), it->getZ(), it->getQuantity());
	}
}

std::list<Objeto> Inventario::getLista()
{
	return lista;
}

void Inventario::setLista(std::list<Objeto> lista1)
{
	lista = lista1;
}

void Inventario::vaciarLista()
{
	lista.clear();
}

int cambiarDeInventario(Objeto obj, Inventario &inv1, Inventario &inv2)
{
	if (inv1.poseeObjeto(obj)) {
		inv1.quitarObjeto(obj);
		inv2.a�adirObjeto(obj);
		return 1;
	}
	else return 0;
}

void descodificarYobtenerObjetos(Inventario * inv, std::string codigo) {
	std::string s = "";
	std::string obj = "";
	int num = 0;
	for (int j = 0; j < codigo.size(); j++) {
		if (codigo[j] == '_') {
			obj = s;
			s = "";
		}
		else if (codigo[j] == '+') {
			num = atoi(s.c_str());
			s = "";
			inv->a�adirObjeto(Objeto(obj, num));
		}
		else
			s += codigo[j];
	}
	num = atoi(s.c_str());
	inv->a�adirObjeto(Objeto(obj, num));
}
