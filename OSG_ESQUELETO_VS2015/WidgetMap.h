#pragma once

#include <osgWidget/Box>

//const unsigned int MASK_2D = 0xF0000000;
//const unsigned int MASK_3D = 0x0F000000;

struct ColorWidget : public osgWidget::Widget {

	ColorWidget(std::string name, float a, float b) :
		osgWidget::Widget(name, a, b) {
		setEventMask(osgWidget::EVENT_ALL);
	}

	bool mouseEnter(double, double, const osgWidget::WindowManager*) {
		//addColor(-osgWidget::Color(0.4f, 0.4f, 0.4f, 0.0f));

		// osgWidget::warn() << "enter: " << getColor() << std::endl;

		return true;
	}

	bool mouseLeave(double, double, const osgWidget::WindowManager*) {
		//addColor(osgWidget::Color(0.4f, 0.4f, 0.4f, 0.0f));

		// osgWidget::warn() << "leave: " << getColor() << std::endl;

		return true;
	}

	bool mouseOver(double x, double y, const osgWidget::WindowManager*) {
		osgWidget::Color c = getImageColorAtPointerXY(x, y);

		if (c.a() < 0.001f) {
			// osgWidget::warn() << "Transparent Pixel: " << x << " " << y << std::endl;

			return false;
		}

		return true;
	}

	bool keyUp(int key, int keyMask, osgWidget::WindowManager*) {
		// osgWidget::warn() << "..." << key << " - " << keyMask << std::endl;

		return true;
	}
};