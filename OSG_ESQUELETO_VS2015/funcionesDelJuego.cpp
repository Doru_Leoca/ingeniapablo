#include "funcionesDelJuego.h"
#include "constantesFisicas.h"
#include "constantesMapa.h"
#include "constantesHeightmap.h"

#include <vector>
#include <Windows.h>
#include "Cargar.h"

std::string porqueEstoyMuerto(std::string fase) {
	std::string razon;
	if (fase == "M1")
		razon = "Jack te ha matado por cotilla";
	else if (fase == "M2")
		razon = "Jack te ha matado para quitarte el paquete";
	else
		razon = "Est�s muerto";
	return razon;
}

void youAreAt(double x, double y, osg::ref_ptr<osg::Image> imagen, const unsigned int puntos)
{
	/*osg::Image* nuevaImagen;
	nuevaImagen = imagen;*/ //c�mo copiarla a otra variable osg::imagen?
	//nuevaImagen->setImage(imagen->s(), imagen->t(), imagen->r(), imagen->getInternalTextureFormat(), /*osg::Texture::InternalFormatMode::USE_IMAGE_DATA_FORMAT,*/ imagen->getPixelFormat(), imagen->getDataType(), imagen->data(), osg::Image::NO_DELETE);
	for (int i=0, R=6; i<=(2*R); i++) {
		for (int j=0; j<=(2*R); j++) {
			double d = sqrt(pow(R - i, 2) + pow(R - j, 2));
			if (d <= R) {
				float xx = (x + puntos/2 + i-R) / puntos;
				float yy = (y + puntos/2 + j-R) / puntos;
				imagen->setColor(COLOR_ROJO, osg::Vec2(xx, yy));
			}
		}
	}
}

void youAreNotAt(float x, float y, osg::ref_ptr<osg::Image> imagen, osg::ref_ptr<osg::Image> imagen2, const unsigned int puntos)
{
	int R=6;
	float xr = x + puntos/2 - R;
	float yr = y + puntos/2 - R;
	for (int i=0; i<=(2*R); i++) {
		for (int j=0; j<=(2*R); j++) {
			float xx = (xr + i) / puntos;
			float yy = (yr + j) / puntos;
			osg::Vec4 color = (imagen->getColor(osg::Vec2(xx, yy)));
			imagen2->setColor(color, osg::Vec2(xx, yy));
		}
	}
}

void youAreAt2(double x, double y, double dir, osg::ref_ptr<osg::Image> imagen, osg::ref_ptr<osg::Image> imagen2)
{
	int H = CURSOR_HEIGHT;
	int W = CURSOR_WIDTH;
	float xx = x + NUM_PUNTOS/2;
	float yy = y + NUM_PUNTOS/2;
	dir -= PI/2;
	for (int i=0; i<W; i++) {
		for (int j=0; j<H; j++) {
			osg::Vec4 color = (imagen->getColor(osg::Vec2(float(i)/W, float(j)/H)));
			if (color.r()<0.1) { //donde haya color negro del cursor
				//con respecto al centro de la imagen
				float ii = i - W/2.0;
				float jj = j - H/2.0;
				//giro
				float iip = ii*cos(dir) - jj*sin(dir);
				float jjp = ii*sin(dir) + jj*cos(dir);
				//coordenadas relativas de la imagen2
				float xr = (xx + iip) / NUM_PUNTOS;
				float yr = (yy + jjp) / NUM_PUNTOS;
				imagen2->setColor(COLOR_AZUL_CLARITO, osg::Vec2(xr, yr));
			}
		}
	}
}

void youAreNotAt2(float x, float y, osg::ref_ptr<osg::Image> imagen, osg::ref_ptr<osg::Image> imagen2)
{
	float H = CURSOR_HEIGHT*1.5; //hace falta restaurar m�s p�xeles de la imagen debido al giro del cursor
	float W = CURSOR_WIDTH*1.5;
	float xx = x + NUM_PUNTOS/2 - W/2;
	float yy = y + NUM_PUNTOS/2 - H/2;
	for (int i=0; i<=W; i++) {
		for (int j=0; j<=H; j++) {
			float xr = (xx + i) / NUM_PUNTOS;
			float yr = (yy + j) / NUM_PUNTOS;
			osg::Vec4 color = (imagen->getColor(osg::Vec2(xr, yr)));
			imagen2->setColor(color, osg::Vec2(xr, yr));
		}
	}
}

std::vector<std::string> get_all_files_names_within_folder(std::string folder)
{
	std::vector<std::string> names;
	std::string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	return names;
}

bool ordenarPorFase(std::pair<std::string,int> p1, std::pair<std::string,int> p2) {
	int a1 = p1.second;
	int a2 = p2.second;
	if (a1 > a2) {
		printf("%d>%d\n", a1, a2);
		return true;
	}
	else
		return false;
}

std::vector<std::pair<std::string,int>> obtenerEstadisticas() {

	std::vector<std::string> archivos = get_all_files_names_within_folder("PartidasGuardadas");
	for (std::vector<std::string>::iterator it = archivos.begin(); it != archivos.end(); ++it) {
		printf("%s\n", (*it).c_str());
	}
	std::vector<std::pair<std::string,int>> clasificacion;
	for (std::vector<std::string>::iterator it = archivos.begin(); it != archivos.end(); ++it) {
		std::string nombreArchivo = (*it).substr(0, (*it).size()-4);
		ArchivosGuardar archivosCargar = Cargar(nombreArchivo);
		clasificacion.push_back(std::make_pair(archivosCargar.NOMBRE, archivosCargar.FASE_JUEGO[0]-'0'));
	}

	for (std::vector<std::pair<std::string,int>>::iterator it = clasificacion.begin(); it != clasificacion.end(); ++it) {
		printf("%s\t%d\n", (*it).first.c_str(), (*it).second);
	}
	std::sort(clasificacion.begin(), clasificacion.end(), ordenarPorFase);
	for (std::vector<std::pair<std::string,int>>::iterator it = clasificacion.begin(); it != clasificacion.end(); ++it) {
		printf("%s\t%d\n", (*it).first.c_str(), (*it).second);
	}

	return clasificacion;
}
