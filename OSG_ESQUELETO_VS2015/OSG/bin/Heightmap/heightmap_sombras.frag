uniform sampler2D texUnit0;	
uniform sampler2D texUnit1;
uniform sampler2D texUnit2;
uniform sampler2D texUnit3;
uniform sampler2D texUnit4;
uniform sampler2D texUnit5;
uniform sampler2D texUnit7;

varying float height;
varying vec3 normal;

varying vec4 shadowCoords;
uniform sampler2DShadow shadowTexture;

float dynamicShadow(){
	return shadow2DProj(shadowTexture,shadowCoords).r; //r, g y b valen lo mismo
}

float proportionTo(in float y, in float value){
	float z;
	if(value==0)
		value=1;
	if(y==0)
		y=1;
	if(y>value){
        float p = (255-value)/(y-value);
        z = y - (255-value)/(p) - (value)/(p);
	}
	else
		z = y;
	float prop = z/value;
    return prop;
}
float distanceTo(in float y, in float value){
	float z;
	if(y<=value)
        z = 255 - (value - y);
	else
		z = 255 + (value - y);
	float dist = z/255;
    return dist;
}
//distanceTo calcula la distancia de un valor a otro y devuelve
//un valor más cercano a 1 cuanto menor sea la distancia.
//a diferencia de proportionTo, el valor devuelto es igual a un
//lado de dicho valor que al otro (estando a la misma distancia)

//float max(){}

void main (void)
{
	vec3 n = normalize(normal);
	float NdotL;
	NdotL = max(dot(n, normalize(vec3(gl_LightSource[0].position))),0.0);
	NdotL = min(NdotL, dynamicShadow());
	
	vec4 t1 = texture2D(texUnit3, gl_TexCoord[0].st * 10);
	vec4 t2 = texture2D(texUnit4, gl_TexCoord[0].st * 10);
	vec4 t3 = texture2D(texUnit5, gl_TexCoord[0].st * 10);
	vec4 t4 = texture2D(texUnit7, gl_TexCoord[0].st * 1000);
	
	vec3 marron = vec3(112,72,19);
	vec3 marron2 = vec3(175,157,132);
	vec3 negro = vec3(0,0,0);
	vec3 rojo = vec3(254,0,0);//tierra
	vec3 verde = vec3(0,254,0);//cesped
	vec3 azul = vec3(0,0,254);//montaña
	vec3 amarillo = vec3(255,255,0);//baldosas
	
	vec4 color = texture2D(texUnit2, gl_TexCoord[0].st);
	float cuantoAmarillo = distanceTo(color.r*255,amarillo.x)*distanceTo(color.g*255,amarillo.y)*distanceTo(color.b*255,amarillo.z);		
	vec4 color_final =  (distanceTo(color.r*255,rojo.x)*distanceTo(color.g*255,rojo.y)*distanceTo(color.b*255,rojo.z))*t1 + 
						(distanceTo(color.r*255,verde.x)*distanceTo(color.g*255,verde.y)*distanceTo(color.b*255,verde.z))*t2 + 
						(distanceTo(color.r*255,azul.x)*distanceTo(color.g*255,azul.y)*distanceTo(color.b*255,azul.z))*t3;
	if(cuantoAmarillo>0.5)
		color_final = cuantoAmarillo*t4;
						
	color_final.rgb= color_final.rgb*(0.4 + NdotL*0.6);
	gl_FragData[0] = color_final;
	
	
}