#pragma once

//para la trayectoria del sol
#define CENTRO_X 0.0
#define CENTRO_Y 0.0
#define CENTRO_Z 0.0
#define RADIO 1200.0
#define ANG_PHI0 25.0	//en grados
#define ANG_THETA0 0.0	//en grados
#define VEL_ANG_PHI 0.001
#define VEL_ANG_THETA 0.0
