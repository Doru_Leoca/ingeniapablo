#pragma once

//para el heightmap
#define HEIGHTMAP_ARCHIVO "./Heightmap/Texturas/Map3.png"//Heightmap.png"
#define HEIGHTMAP_ARCHIVO_NORMALES "./Heightmap/Texturas/Map3_n.png"//heightmap_normals.png"
#define HEIGHTMAP_ARCHIVO_TEXTURAS "./Heightmap/Texturas/Map3_c.png"//heightmap_rgb.png"
#define HEIGHTMAP_TEXTURA_CESPED "./Heightmap/Texturas/cesped.dds"
#define HEIGHTMAP_TEXTURA_NIEVE "./Heightmap/Texturas/snow.dds"
#define HEIGHTMAP_TEXTURA_MONTA�A "./Heightmap/Texturas/mountain.dds"
#define HEIGHTMAP_TEXTURA_DESIERTO "./Heightmap/Texturas/desert.dds"
#define HEIGHTMAP_TEXTURA_TIERRA "./Heightmap/Texturas/tierra2.jpg"
#define HEIGHTMAP_TEXTURA_PIEDRA "./Heightmap/Texturas/stone.dds"
#define HEIGHTMAP_TEXTURA_PAVIMENTO "./Heightmap/Texturas/pavement.jpg"
#define HEIGHTMAP_ARCHIVO_VERT "./Heightmap/heightmap_sombras.vert"
#define HEIGHTMAP_ARCHIVO_FRAG "./Heightmap/heightmap_sombras.frag"
#define HEIGHTMAP_ALTURA 100
#define NUM_PUNTOS 512
#define SEPARACION_PUNTOS 1


