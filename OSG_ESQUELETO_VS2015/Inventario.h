#pragma once

#include "punto.h"
#include <string>
#include <list>
#include <algorithm>

class Objeto : public Punto{
	private:
		std::string nombre;
		unsigned int cantidad;
	
	public:
		Objeto();
		Objeto(std::string n, Punto p, unsigned int c);
		Objeto(std::string n, double x, double y, double z, unsigned int c);
		Objeto(std::string n, unsigned int c=0);
		~Objeto();

		void setName(std::string n) { nombre = n; };
		std::string getName() { return nombre; };

		void setQuantity(unsigned int n) { cantidad = n; };
		unsigned int getQuantity() { return cantidad; };
		void addQuantity(unsigned int n) { cantidad += n; };
		void removeQuantity(unsigned int n);
		
		int operator== (const Objeto& b);
};

class Inventario
{
	private:
		std::list<Objeto> lista;

	public:
		Inventario();
		~Inventario();

		void añadirObjeto(Objeto obj);
		int quitarObjeto(Objeto obj);
		int eliminarObjeto(Objeto obj);
		int poseeObjeto(Objeto obj);
		Objeto* encuentraObjeto(Objeto obj);
		void imprimirLista();
		std::list<Objeto> getLista();
		void setLista(std::list<Objeto> l1);
		void vaciarLista();

};

void imprimirLista(std::list<Objeto> lista);

int cambiarDeInventario(Objeto obj, Inventario &inv1, Inventario &inv2);

void descodificarYobtenerObjetos(Inventario *inv, std::string codigo);
