#include "Mapa.h"
#include "constantesMapa.h"
#include "constantesHeightmap.h"
#include "constantesFisicas.h"

Mapa::Mapa()
{
}

Mapa::~Mapa()
{
}

void Mapa::setImagen(osg::ref_ptr<osg::Image> im)
{
	imagen = im;
}

void Mapa::setImagenOriginal(osg::ref_ptr<osg::Image> im)
{
	imagenOriginal = im;
}

void Mapa::setImagenCursor(osg::ref_ptr<osg::Image> im)
{
	imagenCursor = im;
}

osg::ref_ptr<osg::Image> Mapa::getImagen()
{
	return imagen;
}

void Mapa::dibujarFlecha(double x, double y, double dir)
{
	int H = CURSOR_HEIGHT;
	int W = CURSOR_WIDTH;
	float xx = x + NUM_PUNTOS / 2;
	float yy = y + NUM_PUNTOS / 2;
	dir -= PI / 2;
	for (int i = 0; i<W; i++) {
		for (int j = 0; j<H; j++) {
			osg::Vec4 color = (imagenCursor->getColor(osg::Vec2(float(i) / W, float(j) / H)));
			if (color.r()<0.1) { //donde haya color negro del cursor
				//con respecto al centro de la imagen
				float ii = i - W / 2.0;
				float jj = j - H / 2.0;
				//giro
				float iip = ii*cos(dir) - jj*sin(dir);
				float jjp = ii*sin(dir) + jj*cos(dir);
				//coordenadas relativas de la imagen2
				float xr = (xx + iip) / NUM_PUNTOS;
				float yr = (yy + jjp) / NUM_PUNTOS;
				osg::Vec2 pixeles = osg::Vec2(xr, yr);
				imagen->setColor(COLOR_AZUL_CLARITO, pixeles);
				pixelesModificados.push_back(pixeles);
			}
		}
	}
}

void Mapa::desdibujarFlecha(float x, float y)
{
	while(!pixelesModificados.empty())
	{
		osg::Vec2 pixeles = pixelesModificados.back();
		osg::Vec4 color = (imagenOriginal->getColor(pixeles));
		imagen->setColor(color, pixeles);
		pixelesModificados.pop_back();
	}

	//float H = CURSOR_HEIGHT*1.5; //hace falta restaurar m�s p�xeles de la imagen debido al giro del cursor
	//float W = CURSOR_WIDTH*1.5;
	//float xx = x + NUM_PUNTOS / 2 - W / 2;
	//float yy = y + NUM_PUNTOS / 2 - H / 2;
	//for (int i = 0; i <= W; i++) {
	//	for (int j = 0; j <= H; j++) {
	//		float xr = (xx + i) / NUM_PUNTOS;
	//		float yr = (yy + j) / NUM_PUNTOS;
	//		osg::Vec2 pixeles = osg::Vec2(xr, yr);
	//		osg::Vec4 color = (imagenOriginal->getColor(pixeles));
	//		imagen->setColor(color, pixeles);
	//	}
	//}
}
