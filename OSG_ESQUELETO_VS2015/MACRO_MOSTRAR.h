#pragma once

#ifdef MOSTRAR_CONTROL
#define PRINT_CONTROL(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_CONTROL(mensaje,...)
#endif

#ifdef MOSTRAR_HIERBAS
#define PRINT_HIERBAS(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_HIERBAS(mensaje,...)
#endif

#ifdef MOSTRAR_ARBOLES
#define PRINT_ARBOLES(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_ARBOLES(mensaje,...)
#endif

#ifdef MOSTRAR_DIALOGOS
#define PRINT_DIALOGOS(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_DIALOGOS(mensaje,...)
#endif

#ifdef MOSTRAR_TECLA
#define PRINT_KEY(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_KEY(mensaje,...)
#endif

#ifdef MOSTRAR_MOVIMIENTO
#define PRINT_MOVIMIENTO(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_MOVIMIENTO(mensaje,...)
#endif

#ifdef MOSTRAR_SONIDO
#define PRINT_SONIDO(mensaje,...) printf(mensaje,__VA_ARGS__)
#else
#define PRINT_SONIDO(mensaje,...)
#endif

