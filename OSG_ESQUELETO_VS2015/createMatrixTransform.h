#pragma once

#include <osg/MatrixTransform>
#include "Punto.h"
#define PI 3.14159265358979323846

osg::MatrixTransform* createMatrixTransform(Punto point, double direction, double scale) {
	osg::MatrixTransform* mt= new osg::MatrixTransform;
	/*las dos siguientes l�neas har�an que mtJack y sus hijos no se vieran*/
	//mtJack->setNodeMask( 0x2 ); //only 2nd bit set
	//viewer.getCamera()->setCullMask( 0x1 ); //2nd bit not set
	osg::Matrixd mtras, mrot, mesc, mtotal;
	osg::Vec3d posJack(point.getX(), point.getY(), point.getZ());
	mtras = mtras.translate(posJack);
	mrot = mrot.rotate(direction + PI / 2, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(scale, scale, scale);
	mtotal = mesc*mrot*mtras;
	mt->setMatrix(mtotal);
	return mt;
}