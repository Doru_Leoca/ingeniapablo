#include "Picking.h"

bool PickHandler::handle(const osgGA::GUIEventAdapter & ea, osgGA::GUIActionAdapter & aa)
{
	switch (ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::PUSH):
	{
		osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
		if (view) pick(view, ea);
		return false;
	}
	case(osgGA::GUIEventAdapter::KEYDOWN):
	{
		if (ea.getKey() == 'c')
		{
			osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
			osg::ref_ptr<osgGA::GUIEventAdapter> event = new osgGA::GUIEventAdapter(ea);
			event->setX((ea.getXmin() + ea.getXmax())*0.5);
			event->setY((ea.getYmin() + ea.getYmax())*0.5);
			if (view) pick(view, *event);
		}
		return false;
	}
	default:
		return false;
	}
}

void PickHandler::pick(osgViewer::View* view, const osgGA::GUIEventAdapter& ea)
{
	osgUtil::LineSegmentIntersector::Intersections intersections;

	float x = ea.getX();
	float y = ea.getY();
#if 0
	osg::ref_ptr< osgUtil::LineSegmentIntersector > picker = new osgUtil::LineSegmentIntersector(osgUtil::Intersector::WINDOW, x, y);
	osgUtil::IntersectionVisitor iv(picker.get());
	view->getCamera()->accept(iv);
	if (picker->containsIntersections())
	{
		intersections = picker->getIntersections();
#else
	if (view->computeIntersections(x, y, intersections))
	{
#endif
		for (osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin(); hitr != intersections.end(); ++hitr)
		{
			//std::ostringstream os;
			std::string os;
			//{
			//	osg::ref_ptr<osg::Node> node = hitr->nodePath.at(4);//aqu� da fallo
			//	if (node != NULL) {
			//		os += "Object:" + node->getName() + "\n";
			//	}
			//}
			{
				osg::ref_ptr<osg::Node> last = hitr->nodePath.back();
				int i = 0;
				osg::ref_ptr<osg::Node> it = hitr->nodePath.at(i);
				os += "Path:";
				while (it != last) {
					os += it->getName() + "_";
					i++;
					it = hitr->nodePath.at(i);
				}
				//os += it->getName() + "\n"; //comentado para no a�adir el nombre del �ltimo nodo (la geometr�a)
			}

			path += os;
		}
	}

	}
