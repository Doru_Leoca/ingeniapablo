#include "WidgetMenu.h"
#include "constantesObjetos.h"
#include "constantesNumeros.h"

SonidoLabel::SonidoLabel(const char * label) :
	osgWidget::Label("", "") {
	name = label;
	setFont("./Otros/fonts/times.ttf");
	setFontSize(14);
	setFontColor(COLOR_NEGRO);
	setColor(COLOR_VERDE);
	addHeight(25.0f);
	setCanFill(true);
	setLabel("Sonido");
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_TOP);
	setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
}

void SonidoLabel::setOn() {
	encendido = true;
	setColor(COLOR_VERDE);
	setLabel(name + ": on");
}

void SonidoLabel::setOff() {
	encendido = false;
	setColor(COLOR_ROJO);
	setLabel(name + ": off");
}

void SonidoLabel::reiniciarClick() {
	clicado = false;
}

bool SonidoLabel::mousePush(double, double, const osgWidget::WindowManager *) {
	clicado = true;
	printf("clicado: %s\n", getLabel().c_str());
	return true;
}

bool SonidoLabel::mouseEnter(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_BLANCO);
	return true;
}

bool SonidoLabel::mouseLeave(double, double, const osgWidget::WindowManager *) {
	if (encendido)
		setColor(COLOR_VERDE);
	else
		setColor(COLOR_ROJO);
	return true;
}

std::string ajustarString(std::string s, int n) {
	std::string r = "";
	for (int k = 0, j = 0; j < s.size(); j++) {
		k++;
		if (s[j] == ' ' && k>n) {
			r += "\n";
			k = 0;
		}
		else
			r += s[j];
	}
	return r;
}

osgWidget::Label * createLabel(const std::string & text) {
	osgWidget::Label* label = new osgWidget::Label("", "");

	label->setFont("./Otros/fonts/Vera.ttf");
	label->setFontSize(25);
	label->setFontColor(COLOR_BLANCO);
	label->setLabel(text);

	label->setPadding(10.0f);
	label->addSize(21.0f, 22.0f);

	label->setColor(COLOR_AZUL);

	/*
	text->setBackdropType(osgText::Text::DROP_SHADOW_BOTTOM_RIGHT);
	text->setBackdropImplementation(osgText::Text::NO_DEPTH_BUFFER);
	text->setBackdropOffset(0.2f);
	*/

	return label;
}

ColorLabel::ColorLabel(const char * label) :
	osgWidget::Label("", "") {
	setFont("./Otros/fonts/times.ttf");
	setFontSize(14);
	setFontColor(COLOR_NEGRO);
	setColor(COLOR_GRIS);
	addHeight(25.0f);
	setCanFill(true);
	setLabel(label);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_TOP);
	setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
}

void ColorLabel::reiniciarClick() {
	clicado = false;
}

bool ColorLabel::mousePush(double, double, const osgWidget::WindowManager *) {
	clicado = true;
	printf("clicado: %s\n", getLabel().c_str());
	return true;
}

bool ColorLabel::mouseEnter(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_BLANCO);
	overme = true;
	return true;
}

bool ColorLabel::mouseLeave(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_GRIS);
	overme = false;
	return true;
}

ColorLabelMenu::ColorLabelMenu(const char * label) :
	ColorLabel(label) {
	setFont("./Otros/fonts/dirtydoz.ttf");
	setColor(COLOR_GRIS_CLARITO);
	_window = new osgWidget::Box(
		std::string("Menu_") + label,
		osgWidget::Box::VERTICAL,
		true
	);
	c0 = new ColorLabel(OPCION0); c0->setColor(osg::Vec4(1.0f, 1.0f, 1.0f, 0.0f));
	c1 = new ColorLabel(OPCION1);
	c2 = new ColorLabel(OPCION2);
	c3 = new ColorLabel(OPCION3);
	c4 = new ColorLabel(OPCION4);
	c5 = new ColorLabel(OPCION5);

	_window->addWidget(c4);
	_window->addWidget(c3);
	_window->addWidget(c5);
	_window->addWidget(c2);
	_window->addWidget(c1);
	_window->addWidget(c0);

	_window->setAnchorVertical(osgWidget::Window::VA_TOP);
	_window->setAnchorHorizontal(osgWidget::Window::HA_RIGHT);

	_window->resize();

	setColor(0.5f, 0.5f, 1.0f, 0.8f);
}

void ColorLabelMenu::reiniciarClick(int n) {
	if (n == 0)
		c0->clicado = false;
	else if (n == 1)
		c1->clicado = false;
	else if (n == 2)
		c2->clicado = false;
	else if (n == 3)
		c3->clicado = false;
	else if (n == 4)
		c4->clicado = false;
	else if (n == 5)
		c5->clicado = false;
	else {
		c0->clicado = false;
		c1->clicado = false;
		c2->clicado = false;
		c3->clicado = false;
		c4->clicado = false;
		c5->clicado = false;
	}
}

void ColorLabelMenu::managed(osgWidget::WindowManager * wm) {
	osgWidget::Label::managed(wm);

	wm->addChild(_window.get());

	_window->hide();
}

void ColorLabelMenu::positioned() {
	osgWidget::Label::positioned();

	_window->setOrigin(getX(), getHeight());
	_window->resize(getWidth());
}

bool ColorLabelMenu::mousePush(double, double, const osgWidget::WindowManager *) {
	if (!_window->isVisible())
		_window->show();
	else
		_window->hide();

	/*if (c5->clicado)
	printf("c5 ha sido clicado\n");*/

	return true;
}

bool ColorLabelMenu::mouseLeave(double, double, const osgWidget::WindowManager *) {
	if (!_window->isVisible()) {
		setColor(0.5f, 0.5f, 1.0f, 0.8f);
	}
	/*else {
	bool out = false;
	if(_window->get...)
	out=true
	}*/

	return true;
}

bool ColorLabelMenu::mouseEnter(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_VERDE);

	return true;
}

MenuLabel::MenuLabel(const char * label) :
	osgWidget::Label("", "") {
	setFont("./Otros/fonts/Vera.ttf");
	setFontSize(25);
	setFontColor(COLOR_BLANCO);
	setLabel(label);
	setColor(COLOR_AZUL);
	setPadding(10.0f);
	addSize(21.0f, 22.0f);
	addHeight(50.0f);
	addWidth(200.0f);
	setCanFill(true);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_TOP);
	setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
}

void MenuLabel::reiniciarClick()
{
	clicado = false;
}

bool MenuLabel::mousePush(double, double, const osgWidget::WindowManager *) {
	std::string label = getLabel();
	printf("clicado: %s\n", label.c_str());
	clicado = true;
	return true;
}

bool MenuLabel::mouseEnter(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_AZUL_CLARITO);
	return true;
}

bool MenuLabel::mouseLeave(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_AZUL);
	return true;
}

PreguntaLabel::PreguntaLabel(const char * pers, const char * frase, float length, float height) :
	osgWidget::Label("", "") {
	setFont("./Otros/fonts/Vera.ttf");
	setFontSize(15);
	setFontColor(COLOR_AZUL);
	std::string texto(pers);
	texto += ": ";
	texto += frase;
	setLabel(texto);
	setColor(COLOR_NARANJA);
	setPadding(5.0f);
	//addSize(10.0f, 10.0f);
	addHeight(height);
	addWidth(length);
	setCanFill(true);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);
}

RespuestaLabel::RespuestaLabel(int r, const char * texto, float length, float height) :
	osgWidget::Label("", "") {
	respuesta = r;
	setFont("./Otros/fonts/Vera.ttf");
	setFontSize(15);
	setFontColor(COLOR_AZUL);
	setLabel(texto);
	setColor(COLOR_SALMON);
	setPadding(5.0f);
	//addSize(21.0f, 22.0f);
	addHeight(height);
	addWidth(length);
	setCanFill(true);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);
	setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
}

void RespuestaLabel::reiniciarClick() {
	clicado = false;
}

bool RespuestaLabel::mousePush(double, double, const osgWidget::WindowManager *) {
	std::string label = getLabel();
	printf("clicado: %s\n", label.c_str());
	clicado = true;
	return true;
}

bool RespuestaLabel::mouseEnter(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_BLANCO);
	return true;
}

bool RespuestaLabel::mouseLeave(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_SALMON);
	return true;
}

SentenciaLabel::SentenciaLabel(const char * frase, float length, float height) :
	osgWidget::Label("", "") {
	setFont("./Otros/fonts/Vera.ttf");
	setFontSize(15);
	setFontColor(COLOR_ROJO);
	setLabel(ajustarString(std::string(frase), 20));
	setColor(COLOR_AMARILLO);
	setPadding(5.0f);
	addHeight(height);
	addWidth(length);
	setCanFill(true);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);
}

OpcionLabel::OpcionLabel(const char * texto, float length, float height) :
	osgWidget::Label("", "") {
	setFont("./Otros/fonts/Vera.ttf");
	setFontSize(15);
	setFontColor(COLOR_BLANCO);
	setLabel(texto);
	setColor(COLOR_GRIS_OSCURO);
	setPadding(5.0f);
	addHeight(height);
	addWidth(length);
	setCanFill(true);
	setAlignHorizontal(osgWidget::Widget::HorizontalAlignment::HA_LEFT);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);
	setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
}

void OpcionLabel::reiniciarClick() {
	clicado = false;
}

bool OpcionLabel::mousePush(double, double, const osgWidget::WindowManager *) {
	std::string label = getLabel();
	printf("clicado: %s\n", label.c_str());
	clicado = true;
	return true;
}

bool OpcionLabel::mouseEnter(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_GRIS);
	return true;
}

bool OpcionLabel::mouseLeave(double, double, const osgWidget::WindowManager *) {
	setColor(COLOR_GRIS_OSCURO);
	return true;
}

ObjetoWidget::ObjetoWidget(std::string name, float length, float height) :
	osgWidget::Widget("WIDGETINVENTARIO", length, height) {
	nombre = name;
	setColor(COLOR_BLANCO);
	setPadding(5.0f);
	setCanFill(true);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);

	setImage(osgDB::readImageFile(elegirImagenObjeto(name)));
	setTexCoord(0.0f, 0.0f, osgWidget::Widget::LOWER_LEFT);
	setTexCoord(1.0f, 0.0f, osgWidget::Widget::LOWER_RIGHT);
	setTexCoord(1.0f, 1.0f, osgWidget::Widget::UPPER_RIGHT);
	setTexCoord(0.0f, 1.0f, osgWidget::Widget::UPPER_LEFT);
}

std::string ObjetoWidget::elegirImagenObjeto(std::string name) {
	if (name == "mapa")
		return std::string(OBJETO_MAPA);
	else if (name == "paquete")
		return std::string(OBJETO_PAQUETE);
	else if (name == "lista")
		return std::string(OBJETO_LISTA);
	else if (name == "piezas")
		return std::string(OBJETO_PIEZAS);
	else if (name == "pistola")
		return std::string(OBJETO_PISTOLA);
	else if (name == "bala")
		return std::string(OBJETO_BALA);
	else if (name == "lapiz")
		return std::string(OBJETO_LAPIZ);
	else if (name == "...")
		return std::string(OBJETO_3PUNTOS);
	return std::string(OBJETO_BLANCO);
}

NumeroWidget::NumeroWidget(int number, float l, float h) :
	osgWidget::Widget("WIDGETNUMERO", l, h) {
	numero_ant = numero = number;
	length = l;
	height = h;
	//setColor(COLOR_BLANCO);
	setPadding(5.0f);
	setCanFill(true);
	setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);

	setImage(osgDB::readImageFile(elegirImagenNumero(numero)));
	setTexCoord(0.0f, 0.0f, osgWidget::Widget::LOWER_LEFT);
	setTexCoord(1.0f, 0.0f, osgWidget::Widget::LOWER_RIGHT);
	setTexCoord(1.0f, 1.0f, osgWidget::Widget::UPPER_RIGHT);
	setTexCoord(0.0f, 1.0f, osgWidget::Widget::UPPER_LEFT);
	//setSize(length,height);
}

void NumeroWidget::setNumero(int n) {
	numero_ant = numero;
	numero = n;
	if (numero_ant != numero)
		setImage(osgDB::readImageFile(elegirImagenNumero(numero)));
	//setSize(length,height);
}

std::string NumeroWidget::elegirImagenNumero(int n) {
	switch (n)
	{
	case 0:
		return std::string(NUMERO_0);
		break;
	case 1:
		return std::string(NUMERO_1);
		break;
	case 2:
		return std::string(NUMERO_2);
		break;
	case 3:
		return std::string(NUMERO_3);
		break;
	case 4:
		return std::string(NUMERO_4);
		break;
	case 5:
		return std::string(NUMERO_5);
		break;
	case 6:
		return std::string(NUMERO_6);
		break;
	case 7:
		return std::string(NUMERO_7);
		break;
	case 8:
		return std::string(NUMERO_8);
		break;
	case 9:
		return std::string(NUMERO_9);
		break;
	default:
		return std::string(NUMERO_P);
		break;
	}
}
