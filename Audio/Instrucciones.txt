Utilizando librerías de SDL2 y de SDL2_mixer

Tutorials:
https://www.youtube.com/watch?v=I-yOf4Xg_R8
http://lazyfoo.net/tutorials/SDL/21_sound_effects_and_music/index.php

En propiedades del proyecto:
-añadir la carpeta include a Directorios>Directorios de archivos de inclusión
-añadir la carpeta lib/x64 a Directorios>Directorios de archivos de bibliotecas
-añadir:
SDL2.lib
SDL2main.lib
SDL2_mixer.lib
a Vinculador>Entrada>Dependencias adicionales
-Sistema>Subsitema: Windows (podemos dejarlo como Console Application)

En la carpeta donde vaya a estar el ejecutable: pegar los archivos SDL2.dll y SDL2_mixer.dll

Examples:
https://gist.github.com/armornick/3447121
https://gist.github.com/armornick/3497064

Info:
https://wiki.libsdl.org/APIByCategory
http://sdl.beuc.net/sdl.wiki/SDL_Audio

Archivos de música:
https://opengameart.org/
http://www.wavsource.com/